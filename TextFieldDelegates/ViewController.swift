//
//  ViewController.swift
//  An App: TextFieldDelegates
//
//  Created by Reena Singh on 2/15/15.
//  Copyright (c) 2015 ccsf. All rights reserved.
//

/* TextFieldDelegates: Description

An app with three text fields. The text fields should have the following behaviors

1)  The top text field should allow a maximum of five characters. Any characters acceptable.
2)  The middle text field should display a dollar figure with two digits for the pennies, and at least one digit for the dollars. The field should originally display $0.00. If the user types in five 2’s, then the value should progress like this: $0.02, $0.22, $2.22, $22.22, and finally $222.2.
3)  The last text field should be tied to a switch. When the switch is in the on position the text field should be editable. When it is in the off position the text field should not be editable.

//Run//
1) Simply enter 5 character in first text box.
2) Enter value 99.999 = prints 99.99, backspace to delete, enter 0, re-enter 09999.8888 prints 9999.89, backspace to delete,
enter 0. And start again..enter number of your choice.
3) Let switch be in on position. Enter text-> Reena, Now put switch to off position. Try to enter text. IT is not editable.
 Again put switch in On position. Enter space and Singh. So Will look like Reena Singh. Again put switch at off position. 
Unable to edit further.
//End//

*/

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: Properties
    
    @IBOutlet weak var textField1: UITextField!

    @IBOutlet weak var numField: UITextField!
    
    @IBOutlet weak var numEntered: UILabel!
    
    @IBOutlet weak var textEditOnOff: UITextField!
    
    @IBOutlet weak var OnOffSwitch: UISwitch!
 
    var newText : NSString = ""
   
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        print("view did load")
        OnOffSwitch.isEnabled = true
        textEditOnOff.delegate = self
        
    }
    
    //MARK: UITextFieldDelegate
  
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        // updatedText will take value after edit from first text box, presently initialized to empty string
        var updatedText : NSString = ""
        
        //valueFormat will take value of float after formatting the values enter in second text box, $Amount
        var valueFormat : NSString = ""
     
        // When entered Text upto 5 character in first text Box, the value comes using @IBOutlet UITextField : textField1
        if (textField == textField1){
    
            //count initilize to zero
            var count = 0
            
            // newString variable gets populated from value coming through textField1
            var newString = textField1.text
    
            // Counting character in newString
            for _ in (newString?.characters)!{
    
                // return false if value over 4. count starts at 0,..ends at 4. So will count 5 charater
                if(count >= 4){ return false }
                    count += 1
                }
            
                // updatedText will only allow upto 5 character in first text box..as counting is upto 5 character
                updatedText = (textField1.text! as NSString).replacingCharacters(in: range , with : string) as NSString

                // print used for testing on screen
                print(updatedText)
    
        }
        
        // When entered $Amount in second text Box, the value comes using @IBOutlet UITextField : numField
        
        if(textField == numField){
                
            var showValue : Float = 0
            
            //Initialize tempValue to empty string
            var tempValue = " "
                
            // populating tempValue with numField an UITextField
            tempValue = numField.text!
            
            // Casting tempValue to NSString and then Converting to floatValue
            let ftempValue = (tempValue as NSString).floatValue
            
            // populating showValue variable with ftempValue ( a float value)
            showValue = ftempValue
            
            // value of showValue is now formatted
            valueFormat = NSString(format: "%0.2f", showValue)
            
            /* The valueFormat variable is float, using %0.2f will make the compiler to truncated the value to 2 decimal places*/
            numEntered.text = "$\(valueFormat)"
            
            //This println is used to print value on screen during testing
            print("$\(valueFormat)")
            
        }
        
        // When entered Text in third text Box, the value comes using @IBOutlet UITextField : textEditOnOff
        
        if(textField == textEditOnOff){
            
            // textToEdit variable will take value after edit
            let textToEdit = (textField.text! as NSString).replacingCharacters(in: range , with : string)
                
            newText = textToEdit as NSString
                
                
            if (textToEdit as NSString?) != nil{
                    
                if(OnOffSwitch.isOn){
                    
                    // Text file is editable. Enter character or space or sentence
                    textField.isEnabled = true
                }
                
                if(!OnOffSwitch.isOn){
                    
                    // Text field is not editable( on switch off). The value will be retained from previous state (i.e On)
                    textField.isEnabled = false
                }
                
                // Text field can be enabled again, to enter more value or change value
                textField.isEnabled = true
            }
            
            // print textToEdit on screen ..for testing purpose
            print(textToEdit)
        }
        
        // Return true so the text field will be viewed
        return true
    }

}

